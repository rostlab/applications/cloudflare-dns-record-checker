import os
import cloudflare


def main():
    cf = CloudFlare.CloudFlare(token=os.getenv("CLOUDFLARE_API_TOKEN"))
    zones = cf.zones.get()
    for zone in zones:
        zone_id = zone['id']
        zone_name = zone['name']
        print("zone_id=%s zone_name=%s" % (zone_id, zone_name))

if __name__ == '__main__':
    main()